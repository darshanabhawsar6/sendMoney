import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
class BannerWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 110.0,
        // width: 350.0,
        child: Carousel(
          images: [
            Image.asset("assets/banner1.jpg",fit: BoxFit.cover,),
            Image.asset("assets/baner2.jpg",fit: BoxFit.cover,),
            Image.asset("assets/banner3.png",fit: BoxFit.cover,),

          ],
          dotSize: 6.0,
          dotSpacing: 22.0,
          dotColor: Colors.grey,
          boxFit: BoxFit.cover,
          animationCurve: Curves.fastOutSlowIn,
          indicatorBgPadding: 5.0,
          dotBgColor: Colors.white.withOpacity(0.1),
          borderRadius: false,

        )
    );
  }
}

class CommonWidgets{


  Widget getTextField({String labelText,String hintText}){
    return TextFormField(
      textAlign: TextAlign.start,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: hintText,
        labelText: labelText,
        labelStyle: TextStyle(
            fontWeight: FontWeight.w600
        ),
        suffixIcon: IconButton(
          onPressed: (){},
          icon: Icon(Icons.perm_contact_calendar_sharp),
        )
      ),
    );
  }
}


class Button extends StatelessWidget {
  final String title;
  final Color color;
  final String image;
  final Color textColor;
  final Color borderColor;
  final Function onPressed;
  final double borderRadius;

  const Button({Key key, this.title, this.color = Colors.blue, this.image, this.textColor = Colors.white, this.onPressed, this.borderColor,this.borderRadius = 50}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: RaisedButton(
        elevation: 0,
        color: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
            side: BorderSide(color: borderColor!=null?borderColor:color)),
        onPressed: ()=> onPressed.call(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            image!=null?Padding(
              padding: const EdgeInsets.only(right: 5),
              child: Image.asset(image,
                height: 16,
                width: 16,
                fit: BoxFit.cover,
              ),
            ):SizedBox(),

            Text('$title ',
              style: TextStyle(color: textColor,fontSize: 14),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}