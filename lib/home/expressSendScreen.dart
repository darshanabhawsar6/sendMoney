import 'package:flutter/material.dart';
import 'package:vikingstrength/home/expressSendSecondScreen.dart';
import 'package:vikingstrength/widgets/widgets.dart';

class ExpressSendScreen extends StatefulWidget {
  @override
  _ExpressSendScreenState createState() => _ExpressSendScreenState();
}

class _ExpressSendScreenState extends State<ExpressSendScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        leading:  IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.white,),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Express Send",style: TextStyle(
            color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold
        ),),
      ),
      body: SingleChildScrollView(
        child: Column(
         crossAxisAlignment: CrossAxisAlignment.start,


          children: [
            BannerWidget(),

            Padding(
              padding: const EdgeInsets.only(left: 30,right: 30,top:35.0),
              child: CommonWidgets().getTextField(
                  labelText: "Send to*",
                  hintText:"Enter Mobile Number",),
            ),
            SizedBox(height: 40,),
            Center(
              child: Icon(Icons.dialpad,color: Colors.black87,),
            ),
            Center(
              child: Text("Enter Number to Transfer",style: TextStyle(
                color: Colors.black87,fontWeight: FontWeight.bold,fontSize: 14
              ),),
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Type phone number or tap",style: TextStyle(
                    color: Colors.black38,fontWeight: FontWeight.w500,fontSize: 14
                ),),
                Padding(
                  padding: const EdgeInsets.only(bottom:15.0),
                  child: Icon(Icons.perm_contact_calendar_sharp,color: Colors.black38),
                ),
                Text("to open your contact list",style: TextStyle(
                    color: Colors.black38,fontWeight: FontWeight.w500,fontSize: 14
                ),),
              ],
            ),
          //  Expanded(child: Container(),),
            SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.only(left:30.0,right: 30),
              child: Button(title: "NEXT",
              onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (_) => ExpressSendSecondScreen())),
                ),
            ),
            SizedBox(height: 20,),
          ],
        ),
      ),
    );
  }
}
