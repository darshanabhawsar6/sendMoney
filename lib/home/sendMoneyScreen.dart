import 'dart:ffi';

import 'package:flutter/material.dart';

import 'package:vikingstrength/home/expressSendScreen.dart';
import 'package:vikingstrength/widgets/widgets.dart';

class SendMoneyScreen extends StatefulWidget {
  @override
  _SendMoneyScreenState createState() => _SendMoneyScreenState();
}

class _SendMoneyScreenState extends State<SendMoneyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

       appBar: AppBar(
         backgroundColor: Colors.blueAccent,
         leading:  IconButton(
           icon: Icon(Icons.arrow_back,color: Colors.white,),
           tooltip: 'Back',
           onPressed: () => Navigator.of(context).pop(),
         ),
        title: Text("Send Money",style: TextStyle(
          color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold
        ),),
),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

         BannerWidget(),
          Container(
            width: double.infinity,
            color: Colors.grey.withOpacity(0.2),
            child: Padding(
              padding: const EdgeInsets.only(top:20.0,bottom: 8,left: 15),
              child: Text("SEND MONEY TO A QRTECH ACCOUNT",style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 14
              ),),
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10,),

                InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (_) => ExpressSendScreen()));

                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Icon(Icons.send_to_mobile,color: Colors.blue,size: 30,),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("Express Send",style: TextStyle(
                            fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black38
                        ),),
                      ),
                      SizedBox(height: 5,),
                      Expanded(
                        flex: 0,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Icon(Icons.keyboard_arrow_right,color: Colors.black38,size: 30,),
                        ),
                      ),

                    ],
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Icon(Icons.send_and_archive,color: Colors.blue,size: 30,),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Text("Send with a Clip",style: TextStyle(
                          fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black38
                      ),),
                    ),
                    SizedBox(height: 5,),
                    Expanded(
                      flex: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Icon(Icons.keyboard_arrow_right,color: Colors.black38,size: 30,),
                      ),
                    ),

                  ],
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Icon(Icons.card_giftcard_sharp,color: Colors.blue,size: 30,),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Text("Send Pamasko",style: TextStyle(
                          fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black38
                      ),),
                    ),
                    SizedBox(height: 5,),
                    Expanded(
                      flex: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Icon(Icons.keyboard_arrow_right,color: Colors.black38,size: 30,),
                      ),
                    ),

                  ],
                ),

              ],
            ),
          ),
          Container(
            width: double.infinity,
            color: Colors.grey.withOpacity(0.2),
            child: Padding(
              padding: const EdgeInsets.only(top:20.0,bottom: 8,left: 15),
              child: Text("SEND MONEY TO BANK ACCOUNT",style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 14
              ),),
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 2,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Icon(Icons.account_balance_outlined,color: Colors.blue,size: 30,),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Text("Send to Bank",style: TextStyle(
                          fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black38
                      ),),
                    ),
                    SizedBox(height: 5,),
                    Expanded(
                      flex: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Icon(Icons.keyboard_arrow_right,color: Colors.black38,size: 30,),
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 2,),

              ],
            ),
          ),
          Expanded(
            child: Container(
            color: Colors.grey.withOpacity(0.2),

            ),
          ),
        ],
      ),
    );
  }
}
