import 'package:flutter/material.dart';
import 'package:vikingstrength/home/profileScreen.dart';
import 'package:vikingstrength/widgets/widgets.dart';

class ExpressSendSecondScreen extends StatefulWidget {
  @override
  _ExpressSendSecondScreenState createState() => _ExpressSendSecondScreenState();
}

class _ExpressSendSecondScreenState extends State<ExpressSendSecondScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        leading:  IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.white,),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Express Send",style: TextStyle(
            color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold
        ),),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BannerWidget(),
            Padding(
              padding: const EdgeInsets.only(left: 30,right: 30,top:35.0),
              child: CommonWidgets().getTextField(
                  labelText: "Send to*",
                  hintText:"Enter Mobile Number",),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30,right: 30,),
              child: TextFormField(
                textAlign: TextAlign.start,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "PHP",
                    labelText: "Amount",
                    labelStyle: TextStyle(
                        fontWeight: FontWeight.w600
                    ),

                ),
              )
            ),
            Padding(
              padding: const EdgeInsets.only(left:30.0,top: 1.0),
              child: Text("Available Balance: PHP 5.50",style: TextStyle(
                  color: Colors.black54,fontWeight: FontWeight.bold,fontSize: 14
              ),),
            ),
            Padding(
                padding: const EdgeInsets.only(left: 30,right: 30,),
                child: TextFormField(
                  textAlign: TextAlign.start,
                  keyboardType: TextInputType.text,
                  maxLength: 60,
                  decoration: InputDecoration(
                    hintText: "(Optional)",
                    labelText: "Message",
                    labelStyle: TextStyle(
                        fontWeight: FontWeight.w600
                    ),

                  ),
                )
            ),

            //  Expanded(child: Container(),),
            SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.only(left:30.0,right: 30),
              child: Button(title: "NEXT",
                onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (_) => ProfileScreen())),
              ),
            ),
            SizedBox(height: 20,),
          ],
        ),
      ),
    );
  }
}
