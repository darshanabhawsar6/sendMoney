import 'package:flutter/material.dart';
import 'package:vikingstrength/widgets/widgets.dart';

import 'expressSendSecondScreen.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        leading:  IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.white,),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Express Send",style: TextStyle(
            color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold
        ),),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.blueAccent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Stack(
                    children: [
                      Icon(Icons.circle,size: 100,color: Colors.black38,),
                      Positioned(
                          top: 20,left: 25,
                          child: Icon(Icons.person_outline,size: 50,color: Colors.white,)),
                    ],
                  ),
                ),
                Center(
                  child: Text("ALLAN B.",style: TextStyle(
                      color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16
                  ),),
                ),
                Center(
                  child: Text("09760090232",style: TextStyle(
                      color: Colors.white,fontWeight: FontWeight.normal,fontSize: 16
                  ),),
                ),
                SizedBox(height: 15,)
              ],
            ),
          ),
          Container(
            width: double.infinity,
            color: Colors.grey.withOpacity(0.2),
            child: Padding(
              padding: const EdgeInsets.only(top:20.0,bottom: 12,left: 15),
              child: Text("PAY WITH",style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 16
              ),),
            ),
          ),
          Container(
            width: double.infinity,
            color: Colors.white,
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top:20.0,bottom: 12,left: 15),
                    child: Text("GCash",style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 16
                    ),),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top:20.0,bottom:0 ,left: 15),
                        child: Text("PHP 5.50",style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                        ),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:2.0,bottom: 12,left: 15),
                        child: Text("Available Balnce",style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                        ),),
                      ),
                    ],
                  ),
                )
              ],
            )
          ),
          Container(
            width: double.infinity,
            color: Colors.grey.withOpacity(0.2),
            child: Padding(
              padding: const EdgeInsets.only(top:20.0,bottom: 12,left: 15),
              child: Text("YOU ARE ABOUT TO SEND",style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 16
              ),),
            ),
          ),
          Container(
              width: double.infinity,
              color: Colors.white,
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(top:20.0,bottom: 12,left: 15),
                      child: Text("Total Amount",style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16
                      ),),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top:20.0,bottom:0 ,left: 15),
                      child: Text("PHP 2.00",style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16
                      ),),
                    ),
                  )
                ],
              )
          ),

          Expanded(
            child: Container(
              color: Colors.grey.withOpacity(0.2),
              child: Column(
                children: [
                  Expanded(child: Container()),
                  Text("The recipient's registered name is shown for validation.",style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.bold,
                      fontSize: 14
                  ),),
                  Text("Please make suree that the information entered is correct.",style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.bold,
                      fontSize: 14
                  ),),
                  Text("We cannot reverse the transaction once submitted.",style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 14
                  ),),
                  Padding(
                    padding: const EdgeInsets.only(left:30.0,right: 30,top: 20),
                    child: Button(title: "SEND PHP 2.00",
                      onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (_) => ExpressSendSecondScreen())),
                    ),
                  ),
                  SizedBox(height: 20,),
                ],
              ),

            ),
          ),

        ],
      ),
    );
  }
}
